CC = gcc
CFLAGS = -g -Wall
LIBS = -lm

calcul: calculate.o main.o
	gcc calculate.o main.o -o calcul $(LIBS) $(CFLAGS)
	# компилирует скомпилированные файлы с переменно LIBS

calculate.o: calculate.c calculate.h
	gcc -c calculate.c $(CFLAGS)
	# выполняет файл calculate.c

main.o: main.c
	gcc -c main.c $(CFLAGS)
	# выполняет файл main.c

clean:
	-rm calcul *.o
	# удаляет все скомпилированные файлы с помощью calcul
